# cython: language_level=3

from libc.stddef cimport wchar_t

cdef extern from "Python.h":
	wchar_t* PyUnicode_AsWideCharString(object, Py_ssize_t*) except NULL

def foo():
	try:
		PyUnicode_AsWideCharString('\0', NULL)
	except ValueError:
		print('got ValueError as expected')
